package nrl.actorsim.planner;

import nrl.actorsim.domain.PlanningDomain;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class TFDPlannerWorker extends PDDLPlannerWorker {
    public TFDPlannerWorker(PDDLPlannerOptions options) {
        super(options);
    }

    public static PDDLPlannerOptions createDefaultOptions(PlanningDomain domain) {
        PDDLPlannerOptions options = PDDLPlannerOptions.builder()
                .shortName("TFDThread")
                .domain(domain)
                .build();
        options.pddlPlannerExec.setValue("actorsim-tfd-plan");
        options.plannerTimeoutInSeconds.setValue(30);
        options.parseSolutionFile.enable();

        Path tfdPath = findTfdResourcesPath();
        if (tfdPath == null) {
            tfdPath = findTfdPath();
        }
        if (tfdPath != null) {
            options.pddlPlannerDir.setValue(tfdPath.toAbsolutePath());
        } else {
            logger.error("The TFD planner could not be found");
        }

        return options;
    }


    /**
     * Searches the paths above the current working directory
     * for a path starting with actorsim-planner-tfd and returns
     * the path of tfd within the found exec module or null
     * if no path was found;
     *
     * @return A Planner worker with a found TFD executable.
     */
    @NotNull
    public static TFDPlannerWorker findTFDPlannerWorker(PlanningDomain domain) {
        PDDLPlannerOptions options = TFDPlannerWorker.createDefaultOptions(domain);
        TFDPlannerWorker worker = new TFDPlannerWorker(options);
        logger.info("Established TFD Planner worker {}", worker);
        return worker;
    }


    /**
     * Searches the paths above the current working directory
     * for a path starting with actorsim-planner-tfd and returns
     * the path of tfd within the found exec module or null
     * if no path was found;
     *
     * @return the path to the TFD executable if found
     */
    private static Path findTfdResourcesPath() {
        Path tfdPath = null;
        String tfdRepo = "actorsim-planner-pddl";
        String tfdExecModule = "actorsim-planner-tfd_exec";
        String tfdExecDir = "src/main/resources/tfd-src-0.4/downward";

        Path cwd = Paths.get(System.getProperty("user.dir"));
        Path parent = cwd.getParent();
        while (parent != null) {
            if (parent.toString().contains(tfdRepo)) {
                tfdPath = parent.resolve(tfdExecModule + "/" + tfdExecDir);
                break;
            }

            if (parent.toString().contains(tfdExecModule)) {
                tfdPath = parent.toAbsolutePath();
                break;
            }

            DirectoryStream.Filter<Path> directoryFilter = path -> path.toFile().isDirectory();
            try {
                for (Path path : Files.newDirectoryStream(parent, directoryFilter)) {
                    if (path.toString().contains(tfdRepo)) {
                        tfdPath = path.resolve(tfdExecModule + "/" + tfdExecDir);
                        break;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (tfdPath != null) {
                break;
            }
            parent = parent.getParent();
        }

        return tfdPath;
    }

    /**
     * Searches the paths above the current working directory
     * for a path starting with actorsim-planner-tfd and returns
     * the path of tfd within the found exec module or null
     * if no path was found;
     *
     * @return the path to the TFD executable if found
     */
    private static Path findTfdPath() {
        Path tfdPath = null;
        String tfdParentDir = "workspace";
        String tfdRootDir = "tfd-src-0.4";
        String tfdExecDir = "downward";

        Path cwd = Paths.get(System.getProperty("user.dir"));
        Path parent = cwd.getParent();
        while (parent != null) {
            if (parent.getFileName().toString().contains(tfdParentDir)) {
                tfdPath = parent.resolve(tfdRootDir + "/" + tfdExecDir);
                break;
            }

            if (parent.getFileName().toString().contains(tfdRootDir)) {
                tfdPath = parent.resolve(tfdExecDir);
                break;
            }

            try {
                for (Path dir : Files.newDirectoryStream(parent)) {
                    if (dir.getFileName().toString().contains(tfdParentDir)) {
                        tfdPath = dir.resolve(tfdRootDir + "/" + tfdExecDir);
                        break;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (tfdPath != null) {
                break;
            }
            parent = parent.getParent();
        }

        return tfdPath;
    }

    @Override
    protected void postRunHook() {
        super.postRunHook();
        try {
            String[] args = new String[] {"killall", "search"};
            new ProcessBuilder(args).start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void parseForPlan(List<String> outputLines) {
        currentRequest.resetPlan();
        for (String line : outputLines){
            logger.debug("plan-line("+ outputLines.indexOf(line) + "): {}", line);
            currentRequest.addPlanLine(line);
        }
    }


}

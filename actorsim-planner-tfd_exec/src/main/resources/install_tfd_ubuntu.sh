

rm -rf tfd-src-0.4*
wget http://gki.informatik.uni-freiburg.de/tools/tfd/downloads/version-0.4/tfd-src-0.4.tgz
tar zxf tfd-src-0.4.tgz
cd tfd-src-0.4
sed -e s/"-Werror"//g -i ./downward/search/Makefile
./build

echo "Adding ActorSim's plan file that runs the planner"
#ls -ltr downward/plan
#mv downward/plan downward/plan.original
cp -f ../actorsim-tfd-plan downward
chmod 755 downward/actorsim-tfd-plan
ls -ltr downward/plan

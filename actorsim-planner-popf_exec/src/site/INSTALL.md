# The POPF Connector

We have tested this POPF executable on Ubuntu 16.04.  

## Dependencies

The POPF executable was compiled for a linux 64 bit system.  It requires 
libraries from the COIN OR suite, which can be installed using:
```
sudo apt install coinor-libcbc3
```

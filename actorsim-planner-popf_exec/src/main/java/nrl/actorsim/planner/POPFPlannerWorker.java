package nrl.actorsim.planner;

import nrl.actorsim.domain.PlanningDomain;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static nrl.actorsim.domain.PlanningDomain.NULL_PLANNING_DOMAIN;

public class POPFPlannerWorker extends PDDLPlannerWorker {
    public POPFPlannerWorker(PDDLPlannerOptions options) {
        super(options);
    }

    public static PDDLPlannerOptions createDefaultOptions(PlanningDomain domain) {
        PDDLPlannerOptions options = PDDLPlannerOptions.builder()
                .shortName("POPFThread")
                .domain(domain)
                .build();
        options.pddlPlannerExec.setValue("popf");
        return options;
    }


    /**
     * Searches the paths above the current working directory
     * for a path starting with actorsim-planner-popf and returns
     * the path of popf within the found exec module or null
     * if no path was found;
     *
     * @return A Planner worker with a found POPF executable.
     */
    @NotNull
    public static POPFPlannerWorker findPopfPlannerWorker(PlanningDomain domain) {
        Path popfPath = findPopfPath();
        PDDLPlannerOptions options = POPFPlannerWorker.createDefaultOptions(domain);
        options.pddlPlannerDir.setValue(popfPath.toAbsolutePath());
        return new POPFPlannerWorker(options);
    }

    /**
     * Attempts finding the POPF planner and starting it.
     *
     * @return A Planner worker with a found POPF executable.
     */
    @NotNull
    public static POPFPlannerWorker findAndStartPopfPlannerWorkerWithNullDomain() {
        POPFPlannerWorker worker = findPopfPlannerWorker(NULL_PLANNING_DOMAIN);
        worker.start();
        return worker;
    }

    /**
     * Searches the paths above the current working directory
     * for a path starting with actorsim-planner-popf and returns
     * the path of popf within the found exec module or null
     * if no path was found;
     *
     * @return the path to the POPF executable if found
     */
    private static Path findPopfPath() {
        Path popfPath = null;
        String popfRepo = "actorsim-planner-pddl";
        String popfExecModule = "actorsim-planner-popf_exec";
        String popfExecDir = "src/main/resources/";

        Path cwd = Paths.get(System.getProperty("user.dir"));
        Path parent = cwd.getParent();
        while (parent != null) {
            if (parent.toString().contains(popfRepo)) {
                popfPath = parent.resolve(popfExecModule + "/" + popfExecDir);
                break;
            }

            if (parent.toString().contains(popfExecModule)) {
                popfPath = parent.toAbsolutePath();
                break;
            }

            try {
                for (Path path : Files.newDirectoryStream(parent)) {
                    if (path.toFile().isDirectory()
                            && path.toString().contains(popfRepo)) {
                        popfPath = path.resolve(popfExecModule + "/" + popfExecDir);
                        break;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (popfPath != null) {
                break;
            }
            parent = parent.getParent();
        }

        return popfPath;
    }

}
